// 1. Напишіть функцію, яка повертає частку двох чисел. Виведіть результат роботи функції в консоль.

function validateNumberInput(promptMessage) {
    let input;
    do {
      input = prompt(promptMessage);
      if (isNaN(input)) {
        alert('Помилка: Ви ввели не число. Будь ласка, введіть число.');
      }
    } while (isNaN(input));
    return parseFloat(input);
  }
  
  function divideNumbers(num1, num2) {
    if (num2 === 0) {
      alert('Помилка: Друге число не може бути нулем.');
      return NaN;
    }
    return num1 / num2;
  }
  
  let num1 = validateNumberInput('Введіть перше число:');
  let num2 = validateNumberInput('Введіть друге число:');
  
  let result = divideNumbers(num1, num2);
  console.log('Результат: ' + result);


// 2. Завдання: Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.

function validateNumberInput(promptMessage) {
    let input;
    do {
      input = prompt(promptMessage);
      if (isNaN(input)) {
        alert('Ви ввели не число. Введіть число.');
      }
    } while (isNaN(input));
    return parseFloat(input);
  }
  
  function validateOperationInput(promptMessage) {
    let operation;
    do {
      operation = prompt(promptMessage);
    } while (!['+', '-', '*', '/'].includes(operation));
    return operation;
  }
  
  function performOperation(number1, number2, operation) {
    switch (operation) {
      case '+':
        return number1 + number2;
      case '-':
        return number1 - number2;
      case '*':
        return number1 * number2;
      case '/':
        if (number2 === 0) {
          console.log('Помилка: Друге число не може бути нулем при діленні.');
          return NaN;
        }
        return number1 / number2;
      default:
        alert('Такої операції не існує');
        return NaN;
    }
  }
  
  let number1 = validateNumberInput('Введіть перше число:');
  let number2;
  do {
    number2 = validateNumberInput('Введіть друге число:');
  } while (number2 === 0);
  
  let operation = validateOperationInput('Введіть математичну операцію (+, -, *, /):');
  
  let newResult = performOperation(number1, number2, operation);
  console.log('Результат: ' + newResult);



// 3. Реалізувати функцію підрахунку факторіалу числа.

const factorial = (number) => {
    if (number === 0 || number === 1) {
      return 1;
    } else {
      let result = 1;
      for (let i = 2; i <= number; i++) {
        result *= i;
      }
      return result;
    }
  };

  const userInput = prompt('Введіть число:');

  if (isNaN(userInput)) {
    alert('Введено невірне значення. Будь ласка, введіть число.');
  } else {
    const number = parseInt(userInput);
    const result = factorial(number);
    alert(`Факторіал числа ${number} дорівнює ${result}`);
}

  