1. Як можна сторити функцію та як ми можемо її викликати?

function sayHello() {
  console.log('Привіт! Вітаю з викликом функції.');
}


2. Що таке оператор return в JavaScript? Як його використовувати в функціях?
Оператор return в JavaScript використовується для повернення значення з функції.
Приклад застосування: 

function add(a, b) {
  return a + b;
}
let result = add(3, 5);
console.log(result);


3. Що таке параметри та аргументи в функіях та в яких випадках вони використовуються?
Параметри це змінні, які оголошуються у визначенні функції. Вони вказуються у дужках після 
імені функції і використовуються для прийому даних, які будуть використовуватись усередині функції.

Аргументи це конкретні значення, які передаються у функцію під час її виклику. 
Аргументи вказуються у дужках після імені функції під час її виклику.
